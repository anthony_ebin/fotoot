<?php

namespace App\PaymentGateway;

/**
 * Implements photo repository interface, gets and stores photos for distribution purposes
 */
class BitcoinInfoBiller implements BillerInterface
{
    public function bill()
    {
        // get order details from session/caller
        $productID = 1;
        $price = 0.0002;
        // A custom secret parameter should be included in the callback URL.
        // The secret will be passed back to the callback script when the callback is fired, and should be checked by your code for validity.
        // This prevents someone from trying to call your servers and falsely mark an invoice as 'paid'.
        $secret = $this->random_str(25);
        $my_xpub = getenv('BLOCKCHAIN_XPUB');
        $my_api_key = getenv('BLOCKCHAIN_API_KEY');
        
        
        // call blockchain.info API to generate new address
        $my_callback_url = 'http://mystore.com?order_id=058921123&secret='.$secret;
        $root_url = 'https://api.blockchain.info/v2/receive';
        $parameters = 'xpub=' .$my_xpub. '&callback=' .urlencode($my_callback_url). '&key=' .$my_api_key;
        
        $response = file_get_contents($root_url . '?' . $parameters);
        $object = json_decode($response);
        
        // display new address and callback information to user
        echo 'test';
        echo 'Send Payment To : ' . $object->address;
    }
        
    /**
     * Generate a random string, using a cryptographically secure 
     * pseudorandom number generator (random_int)
     * 
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     * 
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

}

?>