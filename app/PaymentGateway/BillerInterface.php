<?php

    namespace App\PaymentGateway;
    
    /**
     * Interface to bill the user using bitcoin.info or whatever other payment gateway
     *
     * @package PaymentGateway
     * @author `Anthony Ebin`
     */
    interface BillerInterface
    {
        public function bill();
    }

?>