<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
	/**
	* A user can have many products
	*/
	public function products()
	{
		return $this->hasMany('App\Product');
	}
    
	/**
	* A user can upload a product/photo
	*/
	public function publish($product)
	{
		return $this->products()->save($product);
	}
	
}
