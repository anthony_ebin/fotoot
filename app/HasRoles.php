<?php

namespace App;

trait HasRoles {
    /**
     * roles belonging to user
     * @param
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * check if user has role(s)
     * @param
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles()->get()->contains('name', $role);
        }

////		tell me if you have any intersection with the list of roles associated with this user
////		the intersect method allows us to remove any items from $role collection that are not present in $this->roles
////		http://stackoverflow.com/questions/2127260/double-not-operator-in-php
//		return !! $role->intersect($this->roles)->count();

//		A more readable way to do the above, check if the role passed in is one of the current users roles
        foreach ($role as $r) {
            if ($this->hasRole($r->name)) {
                return true;
            }
        }

        return false;

    }

    /**
     * assign role to current user
     * @param $role role to be assigned to user
     */
    public function assignRole($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    /**
     * remove role from current user
     * @param $role role to be assigned to user
     */
    public function removeRole($role)
    {
        return $this->roles()->detach(
            Role::whereName($role)->firstOrFail()->id
        );
    }


}
