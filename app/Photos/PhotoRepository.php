<?php

namespace App\Photos;

use App\Product;
use App\Tag;
use Image;
use Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Implements photo repository interface, gets and stores photos for distribution purposes
 */
class PhotoRepository implements PhotoRepositoryInterface
{
    // baseDir is where all the user uploaded photos are stored. It can be modified to something else if needed
    protected $baseDir = 'products/photos';
    protected $watermarkPath = 'images/watermark.png';
    
    /**
     * function to get photos for index page including possible search query
     * 
     * @param String search query
     */ 
    public function getActivePhotos($searchQuery)
    {
        $products = Product::join('users', 'users.id', '=', 'products.user_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->join('product_tag', 'product_tag.product_id', '=', 'products.id')
            ->join('tags', 'product_tag.tag_id', '=', 'tags.id')
            ->where('roles.name', '=', 'approved_provider');

        // only run this code if there is a search query                
        if(isset($searchQuery) && !empty($searchQuery))
        {
            //trim query so you don't accidentally add a bar to end
            $searchQuery = trim($searchQuery);
            // validate - remove all special char
            $searchQuery = str_replace(' ', '|', $searchQuery); // Replaces all spaces with | to be used in REGEXP function of MYSQL
            //convert to format that can be accepted in SQL IN() or REGEXP function
            $searchQuery = preg_replace("/[^a-zA-Z|]+/", "", $searchQuery);
            
            // if search query is still not empty, i.e. if a number is input at beginning then the search query would be validate to be blank
            if (!empty($searchQuery))
            {
                // append eloquent query to filter results with tags in above imploded query - http://stackoverflow.com/questions/1127088/mysql-like-in
                $products = $products->where('tags.name', 'REGEXP', $searchQuery);
            }
            
        }                        

        // if no images have been added then redirect back
        if(count($products))
        {
            //pass only id and thumbnail path
            $products = $products->paginate(30, ['products.id', 'products.thumbnail_path', 'products.price']);
            return $products;
        }
        
        return false;
        
    }
    
    /**
     * Function to store photo
     * 
     * @param File accepts file instance with photo
     */ 
    public function storePhoto($photoFile)
    {
        // get the file extension
        $photoExt = $photoFile->getClientOriginalExtension();
        // get the client original name
        $photoOriginalName = $photoFile->getClientOriginalName();
        // set the timestamp
        $timeStamp = time();
        //scramble the fileName for the photo
        $photoName = sha1(
            $timeStamp . $photoOriginalName
        ). '.' .$photoExt;
        // set the resolution
        $imagesizeArray = getimagesize($photoFile);
        $photoRes = $imagesizeArray[0] . 'x' . $imagesizeArray[1];
        // set photo file path
        $photoFilePath = '/' . $this->baseDir . '/' . $photoName;
        $photoThumbPath = '/' . $this->baseDir . '/tn-' . $photoName;
        
        // upload photo to base dir
        $photoFile->move(public_path(). '/' . $this->baseDir, $photoName);
        
        // create thumbnail, store in thumbnail path
        $this->makeThumbnail($photoFilePath, $photoThumbPath);
        
        // get details from photo store it in DB
        $photo = new Product;
        
        
        // set timestamp - can't do fill because this field is guarded against mass update
        $photo->created_at = $timeStamp;
        
        $photo = $photo->fill([
            'name' => $photoName,
            'path' => $photoFilePath,
            'thumbnail_path' => $photoThumbPath,
            'extension' => $photoExt,
            'resolution' => $photoRes
        ]);
        
        // attach photo to user - fk and pk
        Auth::user()->publish($photo);
        
    }
    
    /**
    * Create a thumbnail for the photo
    * 
    * @param String original photo path
    * @param String thumbnail path
    */ 
    protected function makeThumbnail($photoPath, $thumbPath)
    {
        // dd(public_path());
        // create a new Image instance for inserting
        $watermark = Image::make(public_path() . '/' . $this->watermarkPath)
            ->fit(300,200)
            ->save();
        
        Image::make(public_path().$photoPath)
            ->fit(300, 200)
            ->insert($watermark, 'center')
            ->save(public_path().$thumbPath);
    }
    
    /**
     * takes a slug and gets photo based on id
     * 
     * @param String slug contains id of photo in beginning
     * @return Product photo instance
     */ 
    public function getIndividualPhoto($slug)
    {
   		$arr = explode("-", $slug, 2);

		$product_id = $arr[0];
		
		return Product::findOrFail($product_id);
		
    }

    /**
     * Delete photos based on provided array of ids
     * 
     * @param Array ids including whether they have been "checked" for deletion
     */ 
    public function deletePhotosWithIds($ids)
    {
        foreach($ids as $id => $checked)
        {
            $product = Product::findOrFail($id);

            // Only owner can delete
    		if(Gate::denies('edit-product', $product)){
    			return redirect('home');
    		}

            $product->delete();
        }
    }
    
    /**
     * Update photos prices based on provided array of ids
     * 
     * @param Array ids including new prices
     */ 
    public function updatePricesOfIds($ids, $deleteIds)
    {
        foreach($ids as $id => $price)
        {
            // only if image has not been requested for deletion
            if (!isset($deleteIds[$id]))
            {
                $product = Product::findOrFail($id);
                
                $product->price = $price;
                
                $product->save();
            }
            
        }

    }
    
    /**
     * Update photos tags based on provided array of ids
     * 
     * @param Array tags including ids
     */ 
    public function updateTagsOfPhotos($tags, $deleteIds)
    {
        foreach($tags as $id => $nestedtags)
        {
            // only if image has not been requested for deletion
            if (!isset($deleteIds))
            {
                $product = Product::findOrFail($id);
                
                $allTagIds = array();

                // MAX 10 tags per product
                for ($i=0; (count($nestedtags)>10 ? $i<10 : $i < count($nestedtags)); $i++)
                {
                     $tagId = $nestedtags[$i]; 
    
                    if (substr($tagId, 0, 4) == 'new:')
                    {
                    // If the same new tag is added on two or more images then we want the db write to happen only once, 
                    // for subsequent tags we want to pull instead of push 
                      $cleanName = strtolower(preg_replace('/[^A-Za-z\-]/', '', substr($tagId, 4)));
                      try {
                          $newTag = Tag::create(['name' => $cleanName]);
                      } catch (\Illuminate\Database\QueryException $e) {
                          $newTag = Tag::whereName($cleanName)->first();                               
                      }
                    
                      $allTagIds[] = $newTag->id;
                       
                      continue;
                    }
                    $allTagIds[] = $tagId;
                }
                $product->tags()->sync($allTagIds);
            }
        }
    }
    
    /**
     * get all tags to show in update view
     */ 
    public function getAllTags()
    {
        return Tag::all();
    }
    
    /**
     * get current user photos
     */ 
    public function getCurrentUserPhotos()
    {
        return Auth::user()->products()->paginate(30);
    }
}

?>