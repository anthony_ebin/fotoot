<?php

    namespace App\Photos;

    /**
     * Interface to connect the product model and controller
     *
     * @package photos
     * @author `Anthony Ebin`
     */
    interface PhotoRepositoryInterface
    {
        public function getActivePhotos($searchQuery);
        
        public function storePhoto($photoFile);
        
        public function getIndividualPhoto($slug);
        
        public function deletePhotosWithIds($ids);
        
        public function updatePricesOfIds($ids, $deleteIds);
        
        public function updateTagsOfPhotos($tags, $deleteIds);
        
        public function getAllTags();
        
        public function getCurrentUserPhotos();
    }

?>