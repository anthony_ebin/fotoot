<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class Product extends Model
{
    protected $fillable = ['path','name','thumbnail_path','extension','resolution', 'price', 'created_at'];
    
    /**
     * function to delete physical images as well as db record
     * 
     */ 
    public function delete()
    {
        // delete the thumbnails and original images
        File::delete([
            public_path().$this->path,
            public_path().$this->thumbnail_path,
        ]); 
        
        // also delete the record
        parent::delete();
    }
    
    /**
	* A product/photo belongs to a user
	* 
	* @return belongsTo relationship
	*/	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
    
    /**
	* A product/photo belongs to many tags
	* 
	* @return belongsToMany relationship
	*/	
	public function tags()
	{
		return $this->belongsToMany('App\Tag')->withTimestamps();
	}
    
    /**
	* Does this product have tag with tag name
	* 
	* @param string $tagName
	* @return belongsToMany relationship
	*/	
	public function hasTag($tagName)
	{
		return $this->tags()->whereName($tagName)->count()>0;
	}
	
    /**
	* return slug of tags belonging to this product
	* 
	* @return belongsToMany relationship
	*/	
	public function slug()
	{
	   // dd($this->tags()->lists('name'));
		return $this->tags()->orderBy('id')->lists('name')->take(6)->implode('-');
	}
	
	/**
	* return tags belonging to this product formated or slugified as required
	* 
	* @param String delimiter 
	* @return belongsToMany relationship
	*/	
	public function getTags($slug=false, $delimiter=',')
	{
	   if ($slug)
	   {
         $slug = $this->tags()->orderBy('id')->lists('name')->take(6)->implode('-');
	   }
	   else $slug = $this->tags()->orderBy('id')->lists('name')->implode($delimiter);
	   
	   return $slug;
	}
	
	/**
	 * gets the name of the uploader of the product
	 * 
	 * @return String name of uploader
	 */ 
	public function getUploader()
	{
	    return $this->user()->first()->name;
	}
}
