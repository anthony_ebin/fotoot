<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];

    
    /**
	* A tag belongs to many products/photos
	* 
	* @return belongsToMany relationship
	*/	
	public function products()
	{
		return $this->belongsToMany('App\Product');
	}
}
