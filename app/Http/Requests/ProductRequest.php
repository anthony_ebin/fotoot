<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pricesArray = $this->input('prices');

        foreach($pricesArray as $key=>$val)
        {
//          spaces matter when passing parameters to the validator
            $rules["prices.".$key] = 'numeric|min:0.5|max:5';
		}
		
        return $rules;
    }
    
    /**
     * custom messages
     * 
     * @return string
     */
    public function messages()
    {
        $pricesArray = $this->input('prices');

        foreach($pricesArray as $key=>$val)
        {
            $messages["prices.".$key.".numeric"] = 'Prices must be numeric';
            $messages["prices.".$key.".min"] = 'Prices cannot be so low';
            $messages["prices.".$key.".max"] = 'Prices cannot be so high';
        }

        return $messages;
    }
}
