<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Social;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Socialite;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins {
        AuthenticatesAndRegistersUsers::register as aliasregister;
    }
    
	/**
	 *
	 * Override default register method to allow adding role of "client"
	 * http://transmission.vehikl.com/do-more-during-registration-in-laravel-5/
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\Response returns to redirected page defined earlier
	 */
	public function register(Request $request)
	{
	   // dd('test');
		// call the RegistersUsers::postRegister method
		// hold onto the return value for later
		$redirect = $this->aliasregister($request);

		$user = $request->user();

//		You can also send email to the client letting him know that he has registered, here

//		Assigning default role
		$user->assignRole('approved_provider');

		return $redirect;

	}

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        
        //Parent constructors are not called implicitly if the child class defines a constructor.
        //In order to run a parent constructor, a call to parent::__construct() within the child constructor is required
        //In this case we have defined a construct in base controller which creates a globally available variable for user and auth::check
        parent::__construct();
    
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    /**
     * Redirect the user to the FB authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $provider_id = \App\SocialProvider::where('name', $provider)->first()->id;
        
        try {
            $social = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return Redirect::to('auth/'.$provider);
        }
        
        // check for existing account via provider uid
        $social_profile = Social::where('uid', $social->id)->first();
        
        if ($social_profile) {
            // if profile exists - get the user object
            $user = User::find($social_profile->user_id);
        } else {
            // otherwise check for an existing email address
            $user = User::where('email', $social->email)->first();
        }
        
        // If we have a user - log them in
        if ($user) {

            \Auth::login($user);

            // Double check the uid on the social_profile
            // for the case of a different provider
            // if not found add a record
            $social_profile = Social::firstOrCreate([
                'user_id' => $user->id,
                'provider_id' => $provider_id,
                'uid' => $social->id
            ]);

            return redirect('home');

        // Profile does not exist - create a new user account
        } else {

            $user           = new User;
            $user->name     = $social->name;
            $user->email    = $social->email;
            $user->password = bcrypt(substr($social->token, 0, 10));
            $user->save();

            $social_profile = Social::firstOrCreate([
                'user_id' => $user->id,
                'provider_id' => $provider_id,
                'uid' => $social->id
            ]);

            \Auth::login($user);

            // redirect the user and suggest changing their password
            return redirect('password/reset');

            $authUser = $this->findOrCreateUser($user);
        }
    }
    

}
