<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\ProductRequest;
use App\Photos\PhotoRepositoryInterface;
use Session;

class ProductController extends Controller
{
    /**
     * Only authorized users allowed in media except for index and show
     * 
     */ 
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        
        //Parent constructors are not called implicitly if the child class defines a constructor.
        //In order to run a parent constructor, a call to parent::__construct() within the child constructor is required
        //In this case we have defined a construct in base controller which creates a globally available variable for user and auth::check
        parent::__construct();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PhotoRepositoryInterface $photorep)
    {
        $products = $photorep->getActivePhotos($request->input('txtSearch'));

        if ($products) {
            return view('products.index', compact('products'));
        }
        
        return redirect('/');
        
    }

    /**
     * Display a listing of the resource belonging to the current user
     *
     * @return \Illuminate\Http\Response
     */
    public function showAllMine(PhotoRepositoryInterface $photorep)
    {
        $tags = $photorep->getAllTags();
        
        // only photos belonging to approved providers
        $products = $photorep->getCurrentUserPhotos();
        
        return view('home', compact('products', 'tags'));

    }
    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
		if(Gate::denies('approved-provider')){
// 			flash()->overlay('You can\'t edit someone else\'s articles', 'Being a sneaky beaky are we?');
			return redirect('home');
		}

        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PhotoRepositoryInterface $photorep)
    {
        
		if(Gate::denies('approved-provider')){
			return redirect('home');
		}

        // file is required, must be of specified type and less than 10mb, 10000kb
        $this->validate($request, [
           'file'=>'mimes:jpg,png,jpeg|max:10000'
        ]);
        
        $photorep->storePhoto($request->file('file'));
        
        return "Done";
    }
    

    /**
     * Display the specified resource.
     *
     * @param  string  $slug  we will only look at the id at the beginning of the slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug, PhotoRepositoryInterface $photorep)
    {
        $product = $photorep->getIndividualPhoto($slug);
        
        // add product id to cart session
        if(isset($_GET['add']))
        {
            // add item to cart only if not already added
            if(!Session::has('cart', $product->id))
            {
                Session::push('cart', $product->id);
            }
        }
        
        
        if(Gate::denies('product-owner-blocked', $product)){
			return redirect('/');
		}
		
        return view('products.show', compact('product'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, PhotoRepositoryInterface $photorep)
    {
        //if any photos have been marked for deletion then delete them
        if($request->has('deletes'))
        {
            $photorep->deletePhotosWithIds($request->deletes);
        }
        
        // Update prices for images that have not been requested for deletion
        if($request->has('prices'))
        {
            $photorep->updatePricesOfIds($request->prices, $request->deletes);
        }

        // Update tags for images that have not been requested for deletion 
        if($request->has('tags'))
        {
            $photorep->updateTagsOfPhotos($request->tags, $request->deletes);
        }
        
        //return back to page where updates were done
        return back();

    }

}
