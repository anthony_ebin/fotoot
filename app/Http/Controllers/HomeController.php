<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
        
        //Parent constructors are not called implicitly if the child class defines a constructor.
        //In order to run a parent constructor, a call to parent::__construct() within the child constructor is required
        //In this case we have defined a construct in base controller which creates a globally available variable for user and auth::check
        parent::__construct();

    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application homepage.
     *
     * @return Response
     */
    public function show()
    {
        return view('pages.home');
    }
}
