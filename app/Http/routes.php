<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

// put all auth here because session state is required when calling oauth providers
    Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

    Route::get('/', 'HomeController@show');
    
    Route::get('/', function () {
        return view('pages.home');
    });
    
    Route::resource('media', 'ProductController');
    Route::post('media/massupdate', 'ProductController@update');
    Route::get('media/{slug}', 'ProductController@show');
    Route::get('/home', 'ProductController@showAllMine');
    
    Route::resource('order', 'OrderController');
    


});
