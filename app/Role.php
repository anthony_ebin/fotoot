<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * A role can have many permissions
     */ 
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Attach permissions to this role
     * 
     * @param Permission $permission assign to this role
     */ 
    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * @param $query - instance of Role
     * @return mixed collection of users who are approvedprovider
     */
    public function scopeApprovedProviders($query)
    {
        return $query->whereName('approved_provider')->first()->users();
    }

}
