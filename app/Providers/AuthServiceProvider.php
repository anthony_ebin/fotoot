<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        // return true if product belongs to $user
        $gate->define('edit-product', function ($user, $product){
            return $user->id == $product->user_id;
        });
        
        // return true if user is an approved provider
        $gate->define('approved-provider', function ($user){
            return $user->hasRole('approved_provider');
        });
        
        // return true if product owner is not approved
        $gate->define('product-owner-blocked', function ($user, $product){
            return $product->user()->first()->hasRole('approved_provider');
        });
    }
}
