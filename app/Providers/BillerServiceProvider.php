<?php

namespace App\Providers;

use App\PaymentGateway\BillerInterface;
use App\PaymentGateway\BitcoinInfoBiller;
use Illuminate\Support\ServiceProvider;

class BillerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BillerInterface::class, function() {
            return new BitcoinInfoBiller;
        });
    }
}
