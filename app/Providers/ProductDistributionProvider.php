<?php

namespace App\Providers;

use App\Photos\PhotoRepositoryInterface;
use App\Photos\PhotoRepository;
use Illuminate\Support\ServiceProvider;

class ProductDistributionProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PhotoRepositoryInterface::class, function() {
            return new PhotoRepository;
        });
    }
}
