<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('social_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        
        Schema::create('socials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            // id in providers' tables
            $table->string('uid');
            $table->timestamps();
        
            //you don’t want two different users to assign the same, say Facebook, account to their account in your application 
            $table->unique(['user_id', 'provider_id']);
            $table->unique(['provider_id', 'uid']);
        
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('provider_id')->references('id')->on('social_providers')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('socials', function(Blueprint $table)
		{
			$table->dropForeign('socials_user_id_foreign');
			$table->dropForeign('socials_provider_id_foreign');
		});
        Schema::drop('socials');
        Schema::drop('social_providers');
    }
}
