<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolePermissionSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();

        if (null == Role::first())
        {

            Role::create(['name' => 'approved_provider', 'label' => 'Approved Product Providers']);
            Role::create(['name' => 'blocked_provider', 'label' => 'Provider awaiting decision']);
            Role::create(['name' => 'blacklisted_provider', 'label' => 'Blacklisted provider, cannot upload, his contents do not appear.']);

        }

        // if (null == Permission::first())
        // {

        //     Permission::create(['name' => 'edit_any_article', 'label' => 'Allows user to edit any article']);
        //     Permission::create(['name' => 'approve_article', 'label' => 'Allows user to see and approve submitted articles']);
        //     Permission::create(['name' => 'respond_requests', 'label' => 'Allows user to respond to client requests']);
        //     Permission::create(['name' => 'see_requests', 'label' => 'Allows user to see client requests']);

        //     Role::whereName('manager')->first()->givePermissionTo(Permission::whereName('edit_any_article')->first());
        //     Role::whereName('manager')->first()->givePermissionTo(Permission::whereName('approve_article')->first());
        //     Role::whereName('manager')->first()->givePermissionTo(Permission::whereName('respond_requests')->first());
        //     Role::whereName('manager')->first()->givePermissionTo(Permission::whereName('see_requests')->first());

        // }


    }

}