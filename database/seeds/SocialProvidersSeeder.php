<?php

use Illuminate\Database\Seeder;
use App\SocialProvider;

class SocialProvidersSeeder extends Seeder {

    public function run()
    {
        DB::table('social_providers')->delete();

        SocialProvider::create(['name' => 'facebook']);
        SocialProvider::create(['name' => 'google']);
    }

}