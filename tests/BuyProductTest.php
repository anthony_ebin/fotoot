<?php

use App\Product;

class BuyProductTest extends TestCase
{
    /**
     * click on add to cart, add to cart in session
     *
     * @return void
     */
    public function testAddItemToCart()
    {
        // upload two images
        $photo1ID = $this->registerAndUpload('/rome.jpg');
        $this->uploadDummyPhoto('/woman.jpg');
        $photo2ID = Product::orderBy('id', 'desc')->first()->id;

        // add both photos to cart
        $this->visit("media/$photo1ID")
            ->click("Add to cart");
        $this->visit("media/$photo2ID")
            ->click("Add to cart");
        //there is no quantity, so adding twice should have no effect
        $this->visit("media/$photo1ID")
            ->click("Add to cart");
            
        // visit checkout 
        $this->visit("cart")
            ->see($photo1ID)
            ->see($photo2ID)
            // see that total price is as expected
            ->press("checkout")
            // see page is now checkout page
            ->seePageIs("checkout")
            // see that total price is as expected
            ->see("BTC 0.025")
            // see qr code for bitcoin and bitcoin address
            ->see("btcAddressHere");

    }
    
    // test photos have been purchased and reflect in db and access rights
    
    
}
