<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;
use App\Tag;

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    use DatabaseTransactions;
    
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }
    
    /**
     * override of storeInput as follows
     * (I put this in my base TestCase so I could reach it from all my tests)
     * 
     * @param  string  $element
     * @param  string  $text
     * @param  bool    $force for multiple select combo boxes, allows multiple inputs
     * @return $this
     */ 
    public function storeInput($element, $text, $force = false)
    {
        if ($force) {
            $this->inputs[$element] = $text;
            return $this;
        }
        else {
            return parent::storeInput($element, $text);
        }
    }
    
    
    protected function registerDummyAccount()
    {
        $this->visit('register')
        ->type('testeryoyoma', 'name')
        ->type('testeryoyoma@test.com', 'email')
        ->type('testeryoyoma1', 'password')
        ->type('testeryoyoma1', 'password_confirmation')
        ->press('Sign Up');
    }
    
    protected function uploadDummyPhoto($absolutePathToFile)
    {   
        
        $this->visit('/media/create')
            ->see('upload-section')
            ->attach(public_path().$absolutePathToFile, 'file')
            ->press('Submit')
            ->see('Done');
    }
    
    /**
     * registers and uploads image based on path
     * 
     * @param String image path
     * @return int product id
     */ 
    protected function registerAndUpload($imgPath)
    {
        $this->registerDummyAccount();
        $this->uploadDummyPhoto($imgPath);
        return Product::orderBy('id', 'desc')->first()->id;
    }
    
         
     /**
      * generates tags for test purposes
      * 
      * @param  String returnType type of data requested in return, IDs or names or object
      * @return Array  returnsArray of IDs or names or object as specified in input parameter
      */ 
     protected function generateTestTags($returnType='tags', $excess = false)
     {
        $tag1 = Tag::create(['name'=>'testertesterone']);
        $tag2 = Tag::create(['name'=>'testertestertwo']);
        $tag3 = Tag::create(['name'=>'testertesterthree']);
        $tag4 = Tag::create(['name'=>'testertesterfour']);
        $tag5 = Tag::create(['name'=>'testertesterfive']);
        $tag6 = Tag::create(['name'=>'testertestersix']);
        $tag7 = Tag::create(['name'=>'testertesterseven']);
        $tag8 = Tag::create(['name'=>'testertestereight']);
        $tag9 = Tag::create(['name'=>'testertesternine']);
        $tag10 = Tag::create(['name'=>'testertesterten']);
        $tag11 = Tag::create(['name'=>'testertestereleven']);
    
        $package = [$tag1, $tag2, $tag3, $tag4, $tag5, $tag6, $tag7, $tag8, $tag9, $tag10];
        
        if ($returnType=='ID')
        {
            $package = [$tag1->id, $tag2->id, $tag3->id, $tag4->id, $tag5->id, $tag6->id, $tag7->id, $tag8->id, $tag9->id, $tag10->id];
        }
        if ($returnType=='name')
        {
            $package = [$tag1->name, $tag2->name, $tag3->name, $tag4->name, $tag5->name, $tag6->name, $tag7->name, $tag8->name, $tag9->name, $tag10->name];
        }
        // if called to attach more than 10 tags, to test limit
        if ($excess)
        {
            $package = [$tag1->id, $tag2->id, $tag3->id, $tag4->id, $tag5->id, $tag6->id, $tag7->id, $tag8->id, $tag9->id, $tag10->id, $tag11->id];
        }
        return $package;
     }

}
