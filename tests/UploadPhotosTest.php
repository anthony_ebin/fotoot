<?php

use App\Product;
use App\Tag;

class UploadPhotosTest extends TestCase
{
    /**
     * A upload is available for authorized users only
     *
     * @return void
     */
    public function testItBlocksTheUploadPage()
    {
        $this->visit('/media/create')
            ->dontSee('upload-section');
    }
    
    /**
     * Test if single upload works
     *
     * @return void
     */
    public function testAuthUserUploadsPhoto()
    {
        $countAtBegin = Product::get()->count();

        $this->registerDummyAccount();
        $this->uploadDummyPhoto('/woman.jpg');
        
            // ->dump(); 
            
        // check db to make sure write is complete, i.e. there is one more row in DB than when we started
        $this->assertTrue(Product::get()->count()>$countAtBegin);
        
        // test thumbnails are created and images are saved
        $filePath = Product::latest()->first()->path;
        $thumbnailPath = Product::latest()->first()->thumbnail_path;
        
        $this->assertTrue(file_exists(public_path().$filePath));
        $this->assertTrue(file_exists(public_path().$thumbnailPath));
        

    }
    
    /**
     * Test edits and deletes of images
     *
     * @return void
     */
    public function testAuthUserEditsPhotoPrice()
    {
        $photoID1 = $this->registerAndUpload('/rome.jpg'); 

        $this->visit('home')
            ->type(4,'prices['.$photoID1.']')
            ->press('Publish Updates');
        
        $this->assertTrue(Product::latest()->first()->price==4);
        
        // test delete
        $this->visit('home')
            ->check('deletes['.$photoID1.']')
            ->press('Publish Updates');
              
        //   check if deleted
        $this->assertTrue(is_null(Product::find($photoID1)));

    }
    
     /**
      * Test that only pictures uploaded by approved users are visible
      * 
      * @return void
      */
     public function testOnlyApprovedUsersPicturesAreVisible()
     {
         // user with role = approved uploads a pic
         $photoID = $this->registerAndUpload('/woman.jpg');

         // user's pics are visible
         $this->visit('media/'.$photoID)
                ->seePageIs('media/'.$photoID)
                ->see('specs');
         
         // user is no longer approved
         $user= Product::find($photoID)->user()->first();
         $user->removeRole('approved_provider');
         $user->assignRole('blocked_provider');
         
         // user's pics are not visible in show view
         $this->visit('media/'.$photoID)
                ->dontSee('specs');
                
        // visit media only if products have been added        
        $products = DB::select(DB::raw("SELECT * FROM products inner join users on products.user_id = users.id inner join role_user on users.id = role_user.user_id inner join roles on role_user.role_id = roles.id WHERE roles.name='approved_provider'"));
        if(count($products))
        {
             // user's pics are not available in index view
             $this->visit('media/')
                    ->dontSee('media/'.$photoID);
        }
        
     }
     
    /**
     * Test that tags are updated correctly.
     * 1. Create tags
     * 2. Visit edit page and add tags
     * 3. Check that tags appear
     * 4. Visit edit page and remove tags
     * 5. Check that tags appear correctly
     * 
     * @param
     * @return
     */
     public function testAuthUserEditsTagsForPhoto()
     {
        $photoID = $this->registerAndUpload('/woman.jpg');
        
        $tagIDsArray = $this->generateTestTags('ID');
        
        // dd('JUju'. "tags[$photoID][]");
        
        $this->visit('home')
            // ->select($tag3ID, "tags[$photoID][]")
            ->storeInput('tags['.$photoID.']', [$tagIDsArray[2]], true)
            // ->dump()
            ->press('Publish Updates');
            
        //   check if updated
        // dd(Product::find($photoID)->tags()->first()->id);
        $this->assertTrue(Product::find($photoID)->tags()->first()->id==$tagIDsArray[2]);
        
     }
     
     /**
      * Test that slugs appear correctly in slug of product
      * only six tags per slug
      * 
      * @return void
      */
     public function testTagsInSlugOfProduct()
     {
        //add 7 tags to a product
        $photoID = $this->registerAndUpload('/woman.jpg');
    
        $tagIDsArray = $this->generateTestTags('ID');

        $lastTag = Tag::orderBy('id', 'desc')->first();
        $first6TagsSlug = Tag::latest()->orderBy('id')->where('id', '<', $lastTag->id)->lists('name')->take(6)->implode('-');

        // dd($first6TagsSlug);
        
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photoID.']', $tagIDsArray, true)
            ->press('Publish Updates');
        
        // dd(Product::find($photoID)->tags()->lists('name'));
        // Product::find($photoID)->slug();
            
        //visit media page and see 6 of 7 tags in url
        $this->visit('media')
            // ->dump()
            ->see($first6TagsSlug)
            ->dontSee($lastTag);
         
        //verify that page link works
        
     }

     
      /**
      * Test that image details appear correctly in show view
      * 
      * @return void
      */
     public function testImageDetailsInShowView()
     {
        // upload image
        $photoID = $this->registerAndUpload('/rome.jpg');
        $tagIDsArray = $this->generateTestTags('ID');
        
        // attach tags
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photoID.']', $tagIDsArray, true)
            ->press('Publish Updates');
        
        // get details from image or db
        $photo = Product::find($photoID);
        $tags = $photo->tags()->lists('name')->all();
        
        // dd($photo->first()->user()->first()->name);
        
        // visit show view of image
        $this->visit('media/'.$photoID)
        // check that details appear as expected
            ->see($photo->price)
            ->see($photo->views)
            ->see($photo->extension)
            ->see($photo->resolution)
            ->see($photo->getUploader())
            // ->dump()
        // all tags
            ->see(implode(",",$tags));
         
     }
     
    /**
      * Test that product can only have so many tags
      * 
      * @return void
      */
     public function testMaxTagsPerProduct()
     {
        // upload image
        $photoID = $this->registerAndUpload('/rome.jpg');
        $excessTagIDsArray = $this->generateTestTags('ID', true);
        
                // attach tags
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photoID.']', $excessTagIDsArray, true)
            ->press('Publish Updates');
            
        // dd(count(Product::find($photoID)->tags()->get()));
        // check that only max number of tags have been attached
        $this->assertTrue(count(Product::find($photoID)->tags()->get())==10);

     }


}
