<?php

use App\Product;
use App\Tag;

class SearchProductsTest extends TestCase
{
    /**
     * search with a single query
     *
     * @return void
     */
    public function testSearchQuery()
    {
        // upload two images
        $photo1ID = $this->registerAndUpload('/rome.jpg');
        $this->uploadDummyPhoto('/woman.jpg');
        $photo2ID = Product::orderBy('id', 'desc')->first()->id;
        // dd($photo2ID.$photo1ID);
        
        // add 2 distinct tags to each image and one common tag: one, two, three for first image. four, five and three for second image
        $tagIDsArray = $this->generateTestTags();
        $tag1 = $tagIDsArray[0];
        $tag2 = $tagIDsArray[1];
        $tag3 = $tagIDsArray[2];
        $tag4 = $tagIDsArray[3];
        $tag5 = $tagIDsArray[4];
        
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photo1ID.']', [$tag1->id, $tag2->id, $tag3->id], true)
            ->press('Publish Updates');
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photo2ID.']', [$tag4->id, $tag5->id, $tag3->id], true)
            ->press('Publish Updates');
            
        // dd(Product::find($photo1ID)->tags()->lists('name').Product::find($photo2ID)->tags()->lists('name'));
        
        // visit any page and enter search query one in search box
        $this->type($tag1->name, 'txtSearch')
            ->press('btnSearch');
        
        // check that page is media
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag1->name);
        // check that only the first product is visible
        $this->see($tag1->name)
            ->see($tag2->name)
            ->see($tag3->name)
        // check that the second product is not visible
            ->dontSee($tag4->name)
            ->dontSee($tag5->name);
        
        
        // enter search query for five
        $this->type($tag5->name, 'txtSearch')
            ->press('btnSearch');

        // check that only the second product is visible
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag5->name);
        $this->see($tag4->name)
            ->see($tag5->name)
            ->see($tag3->name)
        // check that the first product is not visible
            ->dontSee($tag1->name)
            ->dontSee($tag2->name);
        
        // enter search query for three
        $this->type($tag3->name, 'txtSearch')
            ->press('btnSearch');
        
        // check that both first and second products are visible
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag3->name);
        
        $this->see($tag1->name)
            ->see($tag2->name)
            ->see($tag3->name)
            ->see($tag4->name)
            ->see($tag5->name);
            
        // enter search query for two and five
        $this->type($tag1->name . ' ' . $tag5->name, 'txtSearch')
            ->press('btnSearch');
        
        // check that both first and second products are visible
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag1->name.'%20'.$tag5->name);
        
        $this->see($tag1->name)
            ->see($tag2->name)
            ->see($tag3->name)
            ->see($tag4->name)
            ->see($tag5->name);
            
        // enter search query for two and non-existing tag
        $this->type($tag1->name . ' fattyrandomstring', 'txtSearch')
            ->press('btnSearch');
        
        // check that both first and second products are visible
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag1->name.'%20fattyrandomstring');
        
        $this->see($tag1->name)
            ->see($tag2->name)
            ->see($tag3->name);
    
    }
    
    /**
     * clicking on a tag does a search for that tag
     *
     * @return void
     */
    public function testTagClickSearch()
    {
        // upload two images
        $photo1ID = $this->registerAndUpload('/rome.jpg');
        $this->uploadDummyPhoto('/woman.jpg');
        $photo2ID = Product::orderBy('id', 'desc')->first()->id;

        // add 2 distinct tags to each image and one common tag: one, two, three for first image. four, five and three for second image
        $tagIDsArray = $this->generateTestTags();
        $tag1 = $tagIDsArray[0];
        $tag2 = $tagIDsArray[1];
        $tag3 = $tagIDsArray[2];
        $tag4 = $tagIDsArray[3];
        $tag5 = $tagIDsArray[4];
        
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photo1ID.']', [$tag1->id, $tag2->id, $tag3->id], true)
            ->press('Publish Updates');
        $this->visit('home')
            // custom overridden method to add multiple selects, see testcase.php
            ->storeInput('tags['.$photo2ID.']', [$tag4->id, $tag5->id, $tag3->id], true)
            ->press('Publish Updates');
            
        // visit photo page and click tag1
        $this->visit('/media/'.$photo1ID)
            ->click($tag1->name);

        // check that search for tag1 is conducted, see url
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag1->name);
        // check that only the first product is visible
        $this->see($tag1->name)
            ->see($tag2->name)
            ->see($tag3->name)
        // check that the second product is not visible
            ->dontSee($tag4->name)
            ->dontSee($tag5->name);
        
        
        // visit photo page and click tag1
        $this->visit('/media/'.$photo2ID)
            ->click($tag5->name);

        // check that only the second product is visible
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag5->name);
        $this->see($tag4->name)
            ->see($tag5->name)
            ->see($tag3->name)
        // check that the first product is not visible
            ->dontSee($tag1->name)
            ->dontSee($tag2->name);
        
        // visit photo page and click tag1
        $this->visit('/media/'.$photo1ID)
            ->click($tag3->name);
        
        // check that both first and second products are visible
        $this->seePageIs('/media?btnSearch=Search&txtSearch='.$tag3->name);
        
        $this->see($tag1->name)
            ->see($tag2->name)
            ->see($tag3->name)
            ->see($tag4->name)
            ->see($tag5->name);
            
    }
    
}
