@extends('layout')

@section('content')
<!-- Header hero image begin -->
<section id="hero">

    <header>

        <div class="header">
            <form class="login-form" action="{{ url('/register') }}" method="POST">
            {!! csrf_field() !!}
                <div>
                    <h1>Join Us</h1>
                    <input id="username" name="name" type="text" required placeholder="Username" value="{{ old('name') }}"><br>
                    <input id="email" name="email" type="email" required placeholder="Email" value="{{ old('email') }}"><br>
                    <input id="password" name="password" type="password" required placeholder="Password"><br>
                    <input id="password_confirmation" name="password_confirmation" type="password" required placeholder="Password confirmation"><br>
                    <input type="submit" value="Sign Up">
                </div>
            </form>
            
        @include('errors')
        
        </div>
    </header>

</section><!-- Header hero image end -->

@endsection
