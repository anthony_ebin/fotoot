@extends('layout')

@section('content')

<!-- Header hero image begin -->
<section id="hero">

    <header>

        <div class="header">
            <form class="login-form" action="{{ url('/login') }}" method="POST">
                {!! csrf_field() !!}
                <div>
                    <h1>Log in</h1>
                        <input id="email" name="email" type="email" required placeholder="Email" value="{{ old('email') }}"><br>
                        <input id="password" name="password" type="password" required placeholder="Password"><br>
                        <input type="checkbox" name="remember"> Remember Me
                        <input type="submit" value="Log In">
                        <a href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        <a href="{{ url('/register') }}">New User? Sign Up!</a>
                </div>
            </form>
            
        @include('errors')       
                
        </div>
    </header>

</section><!-- Header hero image end -->

@endsection
