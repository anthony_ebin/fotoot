@extends('layout')

@section('content')
    <div>
        Dashboard
        You are logged in!
    </div>
    
    <!--stock-thumbs start-->
<section id="stock-thumbs">
    <div class="header"><h1>Latest images</h1></div>

    <form method="POST" action="/media/massupdate">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PATCH"/>

        <input type="submit" value="Publish Updates"/>

        @foreach($products as $product)
                
            <input type="checkbox" name="deletes[{{ $product->id }}]"/>
            <span class="currencyinput">$<input type="number" name="prices[{{ $product->id }}]" min="0.5" step="0.1" max="5" value="{{ $product->price }}"/></span>
            
            <select name="tags[{{ $product->id }}][]" id="tags-{{$product->id}}" class="product_tags" multiple>
                @foreach($tags as $tag)
                    <option value="{{ $tag->id }}" {{ $product->hasTag($tag->name)? 'selected': null }}>{{ $tag->name }}</option>
                @endforeach
            </select>
                    
            <div class="card">
                <a class="layer" href="media/{{ $product->id }}" ></a>
                <img alt="cat" class="thumbs" src="{{ $product->thumbnail_path }}">
                <!-- if i don't put a space on the tags then it overflows!!-->
                <div class="tags">{{ $product->getTags() }}</div>
                <div class="subtext"><span class="price">${{ $product->price }}</span><span class="favorites">10&hearts;</span></div>
            </div>
    
        @endforeach

        <input type="submit" value="Publish Updates"/>
    
    </form>
            @include('errors')
        
        <?php echo $products->render(); ?>
        
</section><!--stock-thumbs end-->
    
@endsection

@section('scripts.footer')
	<script>
		$('.product_tags').select2({
			placeholder: 'Add tags',
			tags: true,
			tokenSeparators: [",", " "],
            createTag: function(newTag) {
                return {
                    id: 'new:' + newTag.term,
                    text: newTag.term + ' (new)'
                };
            }
		});
	</script>
@stop

