@extends('layout');


@section('content')

<!--stock thumb START-->
<section>
    <div class="card">
        <a class="layer" href="{{ $product->path }}" data-lity></a>
        <img alt="{{ $product->getTags() }}" class="thumbs" src="{{ $product->thumbnail_path }}">
        <div class="tags">{{ $product->getTags() }}</div>
        <div class="subtext"><span class="price">${{ $product->price }}</span><span class="favorites">10&hearts;</span></div>
    </div>
</section><!--stock thumb END-->

<!-- image details section START-->
<section class="image-details">

    <div class="details-card btn-div">
        <a href="?add" class="btn">Add to cart - ${{ $product->price }}</a>
    </div>
    <div class="details-card">
        <p>Submitted by: {{ $product->getUploader() }}</p>
    </div>
    <div class="details-card">
        <table id="specs">
            <tbody>
                <tr><th>Favorites</th><td>30</td></tr>
                <tr><th>Image type</th><td>{{ $product->extension }}</td></tr>
                <tr><th>Resolution</th><td>{{ $product->resolution }}</td></tr>
                <tr><th>Uploaded</th><td>2 days ago</td></tr>
                <tr><th>Views</th><td>{{ $product->views }}</td></tr>
                <tr><th>Purchases</th><td>21</td></tr>
            </tbody>
        </table>
    </div>
    <div class="details-card">
        <p>Tags:</p>
        <p>
            @foreach ($product->tags()->get() as $tag)
                <a href="/media?btnSearch=Search&txtSearch={{ $tag->name }}">{{ $tag->name }}</a>
            @endforeach
        </p>
    </div>
    <div class="details-card">
        <p>License Information:</p>
        <p>Free for commercial use once purchased</p>
        <p>No Attribution Required</p>
        <p>Limitations</p>
        <ul>
            <li>Images and Videos depicting identifiable persons may not be used for pornographic, unlawful or other immoral purposes, or in a way that can give a bad name to people, or to imply endorsement of products and services by those persons, brands, organisations, etc.</li>
            <li>Since we do not require a written Model Release for each Image or Video that shows identifiable people, We cannot guarantee that You will be able to use the Content for any purpose You like.</li>
            <li>Certain Images or Videos may be subject to additional copyrights, property rights, trademarks etc. and may require the consent of a third party or the license of these rights. Pixabay does not represent or make any warranties that it owns or licenses any of the mentioned, nor does it grant them.</li>
        </ul>
    </div>

</section><!-- image details section END-->


@stop