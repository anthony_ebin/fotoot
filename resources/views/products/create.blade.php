@extends('layout');


@section('content')

    <!-- upload section START-->
    <section class="upload-section">
        <div class="details-card">
            <header><h2>Upload files</h2></header>
            <p>Review our Image Quality Guidelines:</p>
            <ul>
                <li>
                    Only these formats are accepted: JPG, PNG, PSD, AI, and SVG
                </li>
                <li>
                    Upload limit for images up to 40 MB
                </li>
                <li>
                    Images must have at least 1920 pixels on the long dimension.
                </li>
                <li>
                    Do send full-sized images. And don't upsize images, because this results in unacceptable blurring.
                </li>
                <li>
                    Do not upload images on which you do not have the copyright. You will be banned for doing so.
                </li>
                <li>
                    Don't include your name or any copyright marks.
                </li>
                <li>
                    We will screen these uploads to see if they are suitable for distribution. We do not accept any adult oriented images.
                </li>
            </ul>
        </div>

        <div class="details-card">
            <form id="addPhotosForm" method="POST" action="/media" class="dropzone" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!--<button id="submit-all" style="display: none;">Submit all files</button>-->
                <!--<input id="tags" name="tags" type="text" required placeholder="Add tags and separate by commas"><br>-->
                <noscript>
                    <input name="file" accept=".jpg, .jpeg, .png" class="dz-hidden-input" multiple="multiple" type="file">
                </noscript>
                <input id="submit-all" style="display: none;" type="submit" value="Submit"/>
            </form>
            @include('errors')
        </div>
    </section><!-- upload section END-->

@stop

@section('scripts.footer')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
    <script>
        Dropzone.options.addPhotosForm = {
            maxFilesize: 10,
            acceptedFiles: '.jpg, .jpeg, .png',
            maxFiles: 25,
            parallelUploads: 25,
            // Prevents Dropzone from uploading dropped files immediately
            autoProcessQueue: false,
            addRemoveLinks : true,
            init: function() {
                var submitButton = document.querySelector("#submit-all");
                myDropzone = this; // closure
                
                submitButton.addEventListener("click", function(e) {
                //   alert(myDropzone.addedfiles);
                    e.preventDefault();
                    myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                });
                
                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                    submitButton.style.display = "none";
                });
            
                // You might want to show the submit button only when 
                // files are dropped here:
                this.on("addedfile", function() {
                // Show submit button here and/or inform user to click it.
                    submitButton.style.display = "block";
                });
                
                // Set up any event handlers
                this.on('success', function () {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        location.href="/home";
                    }
                });
            
            }
        }
    </script>

@stop