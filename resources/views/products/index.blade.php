@extends('layout')


@section('content')

<!--stock-thumbs start-->
<section id="stock-thumbs">
    <div class="header"><h1>Latest images</h1></div>
    
    @foreach($products as $product)
        <div class="card">
            <a class="layer" href="media/{{ $product->id."-".$product->getTags($slug=true) }}"></a>
            <img alt="{{$product->getTags()}}" class="thumbs" src="{{ $product->thumbnail_path }}">
            <!-- if i don't put a space on the tags then it overflows!!-->
            <div class="tags">{{ $product->getTags() }}</div>
            <div class="subtext"><span class="price">${{ $product->price }}</span><span class="favorites">10&hearts;</span></div>
        </div>

    @endforeach
    
</section><!--stock-thumbs end-->

@stop