<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Fototo - Crowdsourced Stock Photos</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <!--Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Nunito:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css" type="text/css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css"  />

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/css/libs.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>

    <!--Nav START-->
    <nav>
        <ul>
            <li>
                <a id="home-banner" href="/">Fototo</a>
            </li>
            <li>
                <form name="frmSearch" action="/media" method="get">
                    <input name="txtSearch" type="text" required>
                    <input type="submit" name="btnSearch" value="Search">
                </form>
            </li>
            <li>
                <a href="login">Sign In</a>
            </li>
            <li>
                <a href="/media/create">Start Selling</a>
            </li>
            <li>
                <p>Hello, {{ isset($user) ? $user->name : 'Welcome' }}</p>
            </li>
        </ul>
    </nav><!--Nav END-->

    <!--container start-->
    <div id="container">
        @yield('content')
    </div><!--container end-->
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="/js/libs.js"></script>
    @yield('scripts.footer')

</body>
</html>