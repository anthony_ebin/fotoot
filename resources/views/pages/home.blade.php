@extends('layout')

@section('content')

    <!-- Header hero image begin -->
    <section id="hero">
    
        <header>
            <h1>
                Fototo - Crowd Sourced Photo Marketplace
            </h1>
        
            <form id="home-search-bar" action="http://webdevbasics.net/scripts/demo.php" method="get">
                <input id="search-box" name="search-box" type="text" required>
                <input type="submit" value="">
            </form>
        
        </header>
    
    </section><!-- Header hero image end -->
    
    <!-- Explainer section begin -->
    <section id="explainer">
    
        <h1 class="header">Buy crowd-sourced images you can use anywhere.</h1>
        <p>You may download, but not modify or distribute images, and use them royalty-free...</p>
        
    </section><!-- Header hero image end -->

@stop